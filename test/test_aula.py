import sys

sys.path.append("./")
from app import hello_world

# testa se a função Hello_World está retornando o valor esperado
def test_deve_retornar_texto_Hello_World():
    expected = "Hello GitLab CI/CD"
    response = hello_world()
    assert response == expected
