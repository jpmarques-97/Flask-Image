# Changelog

<!--next-version-placeholder-->

## v1.6.1 (2021-07-29)
### Fix
* Arrumando o README.md ([`5d0d557`](https://gitlab.com/jpmarques-97/Flask-Image/-/commit/5d0d5577b0a92821c36f54ae8dea1d83beef4aa3))

## v1.6.0 (2021-07-29)
### Feature
* Testando minor version ([`5788381`](https://gitlab.com/jpmarques-97/Flask-Image/-/commit/578838176866e4afca2c0766189a6398a111eabe))

## v1.5.0 (2021-07-29)
### Feature
* Testando minor version ([`4d8c79a`](https://gitlab.com/jpmarques-97/Flask-Image/-/commit/4d8c79a1cfe5c4681e5972c72af1cf252ba0bb38))

## v1.4.0 (2021-07-29)
### Feature
* Testando minor version ([`9c3a5b3`](https://gitlab.com/jpmarques-97/Flask-Image/-/commit/9c3a5b36ea519e25b55b1951edff5a25aa3e7433))

## v1.3.0 (2021-07-29)
### Feature
* Testando minor version ([`1c2b3b8`](https://gitlab.com/jpmarques-97/Flask-Image/-/commit/1c2b3b856a2ad138ea34745b17cf7bb628c90021))

## v1.2.0 (2021-07-29)
### Feature
* Testando minor version ([`b60a9e9`](https://gitlab.com/jpmarques-97/Flask-Image/-/commit/b60a9e93ee5f9882bdb6ad0fc3afce4f72546d47))
* Testando minor version ([`ff3474c`](https://gitlab.com/jpmarques-97/Flask-Image/-/commit/ff3474c5bb54471874091bd492511f2ee1587cda))

## v1.1.0 (2021-07-29)
### Feature
* Testando minor version ([`82638fc`](https://github.com/jpmarques-97/Flask-Image/commit/82638fcf4abf82d28c2d70927c47efcb5ec2289b))
* Testando minor version ([`b768cb6`](https://github.com/jpmarques-97/Flask-Image/commit/b768cb63347bd7a337c9882ef3a0e2201d91f1c3))
* Testando minor version ([`230bfa3`](https://github.com/jpmarques-97/Flask-Image/commit/230bfa39cf147610aae00ed7058caa2d7921a06d))
* Testando minor version ([`f5616a1`](https://github.com/jpmarques-97/Flask-Image/commit/f5616a15efde3550a1197180459b017a8be1cd8c))
* Testando minor version ([`cfe3f1a`](https://github.com/jpmarques-97/Flask-Image/commit/cfe3f1aa9d5ed9c5b614d9cc7f5375a5f54c43b1))
* Testando minor version ([`12b5ff0`](https://github.com/jpmarques-97/Flask-Image/commit/12b5ff0267fb58e198207f29107956843f0d5bce))

## v1.0.3 (2021-07-29)
### Fix
* Ajustando o python-semantic-release ([`33315f3`](https://gitlab.com/jpmarques-97/Flask-Image/-/commit/33315f3547d6087437195ee27adb6acc92d588ce))
