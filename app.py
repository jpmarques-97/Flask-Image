from flask import Flask

app = Flask(__name__)

# retorna Hello World
@app.route("/")
def hello_world():
    return "Hello GitLab CI/CD"
