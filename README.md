# Read Me

![Project Image](https://gainanov.pro/eng-blog/assets/images/gitlab-ci/gitlab-ci-devops.png)

---

### Table of Contents

- [Description](#description)
- [How To Use](#how-to-use)
- [References](#references)
- [License](#license)
- [Author Info](#author-info)

---

## Description

This is a college project, designed to test gitlab CICD functionalities and deepen studies in the Devops culture.

#### Gitlab CI/CD Pipeline

- Uses Pytest to Check Funcionalities
- Deploy a Docker Image for Gitlab  Container Repository
- Uses Python Semantic Release to Release Tags Based on Commit Message

#### Technologies

- Docker
- Python3
- Flask
- Pytest
- Gitlint
- Python Semantic Release
- Gitlab CI-CD

[Back To The Top](#read-me)

---

## How To Use
This Project is already deploying a docker image, to run you need just have docker on your computer and run the following comand
```html
    docker run -p 5000:5000 registry.gitlab.com/jpmarques-97/flask-image:0.1
```
#### Installation
if you dont have docker on your computer, you can install with the followings commands

```html
    sudo apt-get update
    sudo apt-get remove docker docker-engine docker.io
    sudo apt install docker.io
    sudo systemctl start docker
    sudo systemctl enable docker
```
#### API Reference

```html
    curl http://localhost:5000
```
[Back To The Top](#read-me)

---

## References
 - [Flask Quickstart](https://flask.palletsprojects.com/en/2.0.x/quickstart/) documentation
 - [Docker](https://docs.docker.com/get-started/) documentation

[Back To The Top](#read-me)

---

## License

MIT License

Copyright (c) [2021] [João Paulo M de Oliveira]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[Back To The Top](#read-me)

---

## Author Info
- Linkedin - [@João Paulo Marques](https://www.linkedin.com/in/jo%C3%A3o-paulo-marques-705bb516b/)
- Instagram - [@paulo.marques.13](https://www.instagram.com/paulo.marques.13/)
- GitHub -  [@jpmarques-97](https://github.com/jpmarques-97/)

[Back To The Top](#read-me)
